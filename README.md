# my-workstation

My workstation config

# VENV for fresh ansible

```bash
python -m venv venv/ansible
source venv/ansible/bin/activate
pip install -r requirements.txt
ansible-galaxy install -r requirements.yaml
```

# Run playbook

## GNOME

```bash
ansible-playbook -K workstation-gnome.yaml -b
```

## KDE

```bash
ansible-playbook -K workstation-kde.yaml -b
```

